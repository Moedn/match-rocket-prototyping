# Match-Rocket-Prototyping

Rocket Prototyping with Match Propulsion

![](/images/Ex_1_complete_assembled_rocket.png)


## Examples 
Checkout our examples:
- [Example1](/Example1.md)

## How to Contribute
How?

Just add your thoughts and Ideas via [Issues](https://gitlab.com/osh-academy/match-rocket-prototyping/-/issues/new)

## Legal note
 This documentation is licensed under the [MIT License](/LICENSE.md) 

 These are the [People who contributed](/CONTRIBUTORS.md)
